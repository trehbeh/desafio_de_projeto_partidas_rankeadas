
let totalVictories = calcVictories(101, 0)

while (totalVictories > 0){
    let rank = calcLevel(totalVictories)
    
    console.log(`O Herói tem de saldo de ${totalVictories} está no nível de ${rank}`)
    
    totalVictories = calcVictories(totalVictories, 10)
}

function calcVictories (victories, defeats) {
    // Calculo da quantidade 
    let result = victories - defeats
    // Retorno
    return result
}

function calcLevel(totalVictories) {
    let level = " "
    if (totalVictories <= 10){
        level = "Ferro" // Se vitórias for menor do que 10 = Ferro
    } else if (totalVictories <= 20){
        level = "Bronze" // Se vitórias for entre 11 e 20 = Bronze
    } else if (totalVictories <= 50){
        level = "Prata" // Se vitórias for entre 21 e 50 = Prata
    } else if (totalVictories <= 80){
        level = "Ouro" // Se vitórias for entre 51 e 80 = Ouro
    } else if (totalVictories <= 90){
        level = "Diamante" // Se vitórias for entre 81 e 90 = Diamante
    } else if (totalVictories <= 100){
        level = "Lendário" // Se vitórias for entre 91 e 100= Lendário
    } else {
        level = "Imortal" // Se vitórias for maior ou igual a 101 = Imortal
    }
    return level
}